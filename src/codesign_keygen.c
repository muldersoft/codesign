/******************************************************************************/
/* CodeSign, by LoRd_MuldeR <MuldeR2@GMX.de>                                  */
/* This work has been released under the CC0 1.0 Universal license!           */
/******************************************************************************/

#include "common.h"
#include <string.h>

#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/pem.h>
#include <openssl/evp.h>

#define RSA_EXPONENT 0x10001
#define RSA_KEY_SIZE 15360

int MAIN(int argc, CHAR_T *argv[])
{
    int exit_code = EXIT_FAILURE;
    BIGNUM *exp = NULL;
    RSA *rsa = NULL;
    FILE *file_pubkey = NULL, *file_privkey = NULL;
    const EVP_CIPHER *enc = NULL;
    char *passwd = NULL;

    /*-------------------------------------------------------*/
    /* Check arguments                                       */
    /*-------------------------------------------------------*/

    print_logo("Key Generator");

    if ((argc < 4) || (!argv[1][0]) || (!argv[2][0]) || (!argv[3][0]))
    {
        print_license();
        fputs("Usage:\n", stderr);
        fputs("  codesign_keygen.exe <passwd> <signkey.pub> <signkey.key>\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Open output files                                     */
    /*-------------------------------------------------------*/

    file_pubkey = FOPEN_WR(argv[2]);
    if (!file_pubkey)
    {
        fputs("Error: Failed to open output file for public key!\n\n", stderr);
        goto clean_up;
    }

    file_privkey = FOPEN_WR(argv[3]);
    if (!file_privkey)
    {
        fputs("Error: Failed to open output file for private key!\n\n", stderr);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Read password                                         */
    /*-------------------------------------------------------*/

    if ((argv[1][0] == L'-') && (argv[1][1] == L'\0'))
    {
        passwd = read_line_from_file(stdin, 0);
        if (!passwd)
        {
            fputs("Error: Failed to read password from STDIN stream!\n\n", stderr);
            goto clean_up;
        }
    }
    else
    {
        passwd = convert_CHAR_to_UTF8(argv[1]);
        if (!passwd)
        {
            fputs("Error: Failed to convert password to UTF-8 format!\n\n", stderr);
            goto clean_up;
        }
    }

    if (strlen(passwd) < PASSWD_MINLEN)
    {
        fprintf(stderr, "Error: Password is too short! (min. length: %d)\n\n", PASSWD_MINLEN);
        goto clean_up;
    }

    /*-------------------------------------------------------*/
    /* Generate RSA key-pair                                 */
    /*-------------------------------------------------------*/

    exp = BN_new();
    if (!exp)
    {
        fputs("Error: Failed to allocate exponent!\n\n", stderr);
        goto clean_up;
    }

    if (BN_set_word(exp, RSA_EXPONENT) != 1)
    {
        fputs("Error: Failed to set up exponent!\n\n", stderr);
        goto clean_up;
    }

    rsa = RSA_new();
    if (!rsa)
    {
        fputs("Error: Failed to allocate RSA context!\n\n", stderr);
        goto clean_up;
    }

    fputs("Generating RSA key-pair. Please be patient, this will take a while...\n", stderr);
    fflush(stderr);

    if (RSA_generate_key_ex(rsa, RSA_KEY_SIZE, exp, NULL) != 1)
    {
        fputs("Failed!\n\nError: Failed to generate RAS key!\n\n", stderr);
        goto clean_up;
    }

    if (RSA_size(rsa) != RSAKEY_MINLEN)
    {
        fputs("Failed!\n\nError: RSA key size differes from what was expected!\n\n", stderr);
        goto clean_up;
    }

    fputs("Completed.\n\n", stderr);
    fflush(stderr);

    /*-------------------------------------------------------*/
    /* Write public and private keys to files                */
    /*-------------------------------------------------------*/

    enc = EVP_aes_256_cbc();
    if (!enc)
    {
        fputs("Error: Failed to initialize cipher for key export!\n\n", stderr);
        goto clean_up;
    }

    if (PEM_write_RSAPublicKey(file_pubkey, rsa) != 1)
    {
        fputs("Error: Failed to write the public key file!\n\n", stderr);
        goto clean_up;
    }

    if (!force_flush(file_pubkey))
    {
        fputs("I/O Error: Public key could not be written completely!\n\n", stderr);
        goto clean_up;
    }

    if (PEM_write_RSAPrivateKey(file_privkey, rsa, enc, (unsigned char*)passwd, strlen(passwd), NULL, NULL) != 1)
    {
        fputs("Error: Failed to write the private key file!\n\n", stderr);
        goto clean_up;
    }

    if (!force_flush(file_privkey))
    {
        fputs("I/O Error: Private key could not be written completely!\n\n", stderr);
        goto clean_up;
    }

    fputs("RSA key-pair written successfully.\n\n", stderr);
    exit_code = EXIT_SUCCESS;

    /*-------------------------------------------------------*/
    /* Final clean-up                                        */
    /*-------------------------------------------------------*/

clean_up:

    if (passwd)
    {
        OPENSSL_clear_free(passwd, strlen(passwd));
    }

    if (file_pubkey)
    {
        fclose(file_pubkey);
    }

    if (file_privkey)
    {
        fclose(file_privkey);
    }

    if (rsa)
    {
        RSA_free(rsa);
    }

    if (exp)
    {
        BN_free(exp);
    }

    return exit_code;
}
